package com.example.fingerup;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SelectionCours extends AppCompatActivity {

    String Bearer = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection_cours);
        Intent intent = getIntent();
        Bearer = intent.getStringExtra("Bearer");

    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.Conduite_de_projet:
                Intent intent = new Intent(this, QRCode.class);
                intent.putExtra("id_cours", "728ce5360dd77eaba1c9679858bd9725");
                intent.putExtra("Bearer", Bearer);
                startActivity(intent);
                break;
        }
    }
}
