package com.example.fingerup;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {


    private String Bearer = "";
    private String type = "";


    private TextView monTextView;

    private boolean Auth = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        monTextView = findViewById(R.id.textViewMainActivity);

    }

    public void connexionAction(View view) {

        if(!Auth){
            monTextView.setText("Mauvais Identifiant");
        }

        Log.e("API", "Appuie sur le boutton");

        TextInputEditText monInputTextMail = findViewById(R.id.email);
        TextInputEditText monInputTextPass = findViewById(R.id.mdp);


        String monEmail = monInputTextMail.getText().toString();
        String monPassword = monInputTextPass.getText().toString();



        OkHttpClient client  = new OkHttpClient();
        //String url = "http://c6935889ba02.ngrok.io/api/login";
        String url = "http://192.168.10.10/api/login";

/*
        String email = "timurandu60@gmail.com";
        String password = "root";*/

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", monEmail);
            jsonObject.put("password", monPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "")
                .post(body)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()) {
                    String myResponse = response.body().string();
                    try {
                        JSONObject jsonResult = new JSONObject(myResponse);
                        Bearer = jsonResult.getString("access_token");

                        requeteType();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else{
                }
            }
        });
    }

/*

    public void pasBonId(){
        monTextView.setText("Mauvais identifiants");
    }*/

    public void changeActivite(){

        if(type.equals("student")){

            Intent intent = new Intent(this, ScannerBarcodeActivity.class);
            intent.putExtra("Bearer", Bearer);
            startActivity(intent);
        }else if(type.equals("professor")){

            Intent intent = new Intent(this, SelectionCours.class);
            intent.putExtra("Bearer", Bearer);
            startActivity(intent);
        }
    }


    public void requeteType() {

        OkHttpClient client  = new OkHttpClient();
        //String url = "http://c6935889ba02.ngrok.io/api/user";
        String url = "http://192.168.10.10/api/user";

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");


        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "")
                .addHeader("Authorization", "Bearer " + Bearer)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()) {
                    String myResponse = response.body().string();

                    try {
                        JSONObject jsonResult = new JSONObject(myResponse);

                        type = jsonResult.getString("type");

                        changeActivite();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

}