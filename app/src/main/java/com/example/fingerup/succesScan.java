package com.example.fingerup;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class succesScan extends AppCompatActivity {

    private String Bearer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.succes_scan);

        Intent intent = getIntent();
        Bearer = intent.getStringExtra("Bearer");

    }

    public void retourEcranSuccesScan(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Bearer", Bearer);
        startActivity(intent);

    }


}
