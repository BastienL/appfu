package com.example.fingerup;

import android.content.Intent;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class QRCode extends AppCompatActivity {

    private EditText edtValue;
    private ImageView qrImage;
    private String inputValue = "";
    private Bitmap bitmap;
    private QRGEncoder qrgEncoder;
    private AppCompatActivity activity;

    private String Bearer = "";
    private String id_cours = "";

    private void AppelAPI(){

        OkHttpClient client  = new OkHttpClient();
        String url = "http://192.168.10.10/api/token";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cours_id", id_cours);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "")
                .addHeader("Authorization", "Bearer "+ Bearer)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                changerActiviterEchecGenerationQR();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()) {
                    String myResponse = response.body().string();

                    try {
                        JSONObject jsonResult = new JSONObject(myResponse);
                        inputValue = jsonResult.getString("Token");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    changerActiviterEchecGenerationQR();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code);

        qrImage = findViewById(R.id.id_qr_image);
        activity = this;

        Intent intent = getIntent();
        id_cours = intent.getStringExtra("id_cours");
        Bearer = intent.getStringExtra("Bearer");
        AppelAPI();
        SystemClock.sleep(2000);
        if (inputValue.length() > 0) {
            WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;

            qrgEncoder = new QRGEncoder(
                    inputValue, null,
                    QRGContents.Type.TEXT,
                    smallerDimension);
            qrgEncoder.setColorBlack(Color.BLACK);
            qrgEncoder.setColorWhite(Color.WHITE);

            try {
                bitmap = qrgEncoder.getBitmap();
                qrImage.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            edtValue.setError(inputValue);
        }

    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.id_retour_QRCode:
                Intent intent = new Intent(this, SelectionCours.class);
                startActivity(intent);
                break;
            case R.id.id_compte_QRCode:
                startActivity(new Intent(this, SelectionCours.class));
                break;
        }
    }

    public void changerActiviterEchecGenerationQR(){
            Intent intent = new Intent(this, ScannerBarcodeActivity.class);
            intent.putExtra("Bearer", Bearer);
            startActivity(intent);
    }
}