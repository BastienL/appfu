package com.example.fingerup;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import android.widget.Toast;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;

public class informationCompte extends AppCompatActivity {

    private TextView NameTextView;
    String Bearer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_compte);

        Intent intent = getIntent();
        Bearer = intent.getStringExtra("Bearer");
        NameTextView = findViewById(R.id.nameText);
        get_name();
    }

    private void get_name() {

        OkHttpClient client = new OkHttpClient();
        String url = "http://192.168.10.10/api/user";

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Bearer " + Bearer)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String myResponse = response.body().string();

                //NameTextView.setText("test");
                try {
                    JSONObject jsonResult = new JSONObject(myResponse);
                    NameTextView.setText(jsonResult.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void deconnexionAction(View view) {
        Toast.makeText(getApplicationContext(), "Déconnexion ...", Toast.LENGTH_SHORT).show();

        OkHttpClient client = new OkHttpClient();
        String url = "http://192.168.10.10/api/logout";

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");


        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Bearer " + Bearer)
                .build();

        client.newCall(request);
        changeActivite();
    }

    public void changeActivite(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("logout", "Je viens des informations de compte");
        startActivity(intent);
    }

    public void onClick(View view){

        Intent intent = new Intent(this, ScannerBarcodeActivity.class);
        intent.putExtra("information_compte", "Je viens des informations de compte");
        intent.putExtra("Bearer", Bearer);
        startActivity(intent);


    }

}
