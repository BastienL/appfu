package com.example.fingerup;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ResultatScan extends AppCompatActivity {

    private String Bearer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resultat_scan);

        Intent intent = getIntent();
        String Lecture_QR_Code = intent.getStringExtra("Lecture_QR_Code");
        Bearer = intent.getStringExtra("Bearer");

        OkHttpClient client  = new OkHttpClient();
        //String url = "http://c6935889ba02.ngrok.io/api/onScan";
        String url = "http://192.168.10.10/api/onScan";


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token_id", Lecture_QR_Code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonObject.toString());

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Accept", "")
                .addHeader("Authorization", "Bearer " + Bearer)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                changeActivityEchecScan();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()) {
                    changeActivitySuccessScan();
                }
                else{
                    changeActivityEchecScan();
                }
            }
        });

    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.id_retour_QRCode:
                Intent intent = new Intent(this, ScannerBarcodeActivity.class);
                startActivity(intent);
                break;
            case R.id.id_compte_QRCode:
                startActivity(new Intent(this, ScannerBarcodeActivity.class));
                break;
        }
    }

    public void changeActivitySuccessScan(){


        Intent intent = new Intent(this, succesScan.class);
        intent.putExtra("Bearer", Bearer);
        startActivity(intent);
    }
    public void changeActivityEchecScan(){


            Intent intent = new Intent(this, echecScan.class);
            intent.putExtra("Bearer", Bearer);
            startActivity(intent);
    }
}
